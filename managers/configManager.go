package managers

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/staticvoidlabs/fetchy/models"
)

var mCurrentConfig models.Config

// Public functions.
func ProcessConfigFile() {

	var currentConfig models.Config

	configFile, err := os.Open("./config.json")
	defer configFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)

	mCurrentConfig = currentConfig
}

func GetVersionInfo() string {

	tmpVersionInfo := "n/a"

	if mCurrentConfig.Version != "" {
		tmpVersionInfo = mCurrentConfig.Version
	}

	return tmpVersionInfo
}

func GetRefrehInterval() int32 {

	return mCurrentConfig.RefreshInterval
}
