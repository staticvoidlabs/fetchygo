package managers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/staticvoidlabs/fetchy/models"
)

var mDayOfScope string = ""
var mFilter string = ""

func InitAPIService() {

	WriteLogMessage("Starting Web API services...", true)

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/fetchy/init", initEpgRepo)
	router.HandleFunc("/fetchy/{id}", showEpgData)
	router.HandleFunc("/fetchy/{id}/filter", setDefaultFilter)
	router.HandleFunc("/fetchy/{id}/filter/live", setLiveFlag)
	router.HandleFunc("/fetchy/{id}/filter/{filter}", setAdhocFilter)

	// Prepare router to handle static files.
	fs := http.FileServer(http.Dir("./static/"))
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs))

	// Start rest service.
	tmpConfiguredApiPort := ":" + strconv.Itoa(mCurrentConfig.WebAPIPort)

	err := http.ListenAndServe(tmpConfiguredApiPort, router)

	if err != nil {
		fmt.Println("Error starting Web API services: ")
		fmt.Println(err)
	}

}

// Endpoint implementation.

func showSummary(w http.ResponseWriter, r *http.Request) {

	if mCurrentConfig.ShowSummaryAfterRefresh {

		w.Write([]byte("Summary" + "\r\n"))
		w.Write([]byte("---------------------------------------" + "\r\n"))
		w.Write([]byte("State:      " + mRefreshSummary.RefreshState + "\r\n"))
		w.Write([]byte("Timestamp:  " + "mRefreshSummary.TimeStamp" + "\r\n"))
		w.Write([]byte("Channels:   " + strconv.Itoa(mRefreshSummary.CountChannels) + "\r\n"))
		w.Write([]byte("Show Items: " + strconv.Itoa(mRefreshSummary.CountShowItems) + "\r\n"))
		w.Write([]byte("Filter set: " + mRefreshSummary.FilterSet + "\r\n"))

		w.Write([]byte("\r\n" + "\r\n"))

	}

}

func initEpgRepo(w http.ResponseWriter, r *http.Request) {

	mFilter = mCurrentConfig.WhitelistTitleElements
	GetEpgData()
	showSummary(w, r)

}

func showEpgData(w http.ResponseWriter, r *http.Request) {

	tmpFilterHeader := r.Header.Get("filter")

	switch tmpFilterHeader {
	case "":
		mRefreshSummary.FilterSet = "none"
		mFilter = ""
	case "default":
		mRefreshSummary.FilterSet = "Default (" + mFilter + ")"
	case "adhoc":
		mRefreshSummary.FilterSet = "Adhoc (" + mFilter + ")"
	}

	if len(mEpgRepository.EpgChannelRepos0) < 1 {
		GetEpgData()
	}

	setDayOfScope(r)
	showSummary(w, r)

	var tmpRepoToUse []models.EpgChannelRepo

	switch mDayOfScope {
	case "0":
		tmpRepoToUse = mEpgRepository.EpgChannelRepos0
	case "1":
		tmpRepoToUse = mEpgRepository.EpgChannelRepos1
	case "2":
		tmpRepoToUse = mEpgRepository.EpgChannelRepos2
	default:
		tmpRepoToUse = mEpgRepository.EpgChannelRepos0
	}

	for _, ChannelRepo := range tmpRepoToUse {
		tmpHasRelevantShows, tmpHasRelevantLiveShows := ChannelHasRelevantShowItems(ChannelRepo, mFilterLiveShows)

		if (!mFilterLiveShows && tmpHasRelevantShows) || (mFilterLiveShows && tmpHasRelevantLiveShows) {

			w.Write([]byte(ChannelRepo.ChannelName + "\r\n"))
			w.Write([]byte("---------------------------------------" + "\r\n"))

			for _, showItem := range ChannelRepo.EpgShowItems {

				if mFilter == "" {

					w.Write([]byte(showItem.StartTime + " " + showItem.Title + "\r\n"))

				} else if showItem.Scoring > 0 {

					if (mFilterLiveShows && showItem.Live) || (!mFilterLiveShows) {
						w.Write([]byte(showItem.StartTime + " " + showItem.Title + " (" + strconv.Itoa(showItem.Scoring) + ")" + "\r\n"))
					}

				}

			}

			w.Write([]byte("\r\n" + "\r\n"))
		}

	}

	mFilter = ""
}

func setDefaultFilter(w http.ResponseWriter, r *http.Request) {

	setDayOfScope(r)

	mFilterLiveShows = false

	r.Header.Set("filter", "default")
	mFilter = mCurrentConfig.WhitelistTitleElements

	showEpgData(w, r)
}

func setLiveFlag(w http.ResponseWriter, r *http.Request) {

	setDayOfScope(r)

	mFilterLiveShows = true

	r.Header.Set("filter", "default")
	mFilter = mCurrentConfig.WhitelistTitleElements

	showEpgData(w, r)
}

func setAdhocFilter(w http.ResponseWriter, r *http.Request) {

	setDayOfScope(r)

	mFilterLiveShows = false

	vars := mux.Vars(r)
	r.Header.Set("filter", "adhoc")
	mFilter = vars["filter"]

	showEpgData(w, r)
}

func setDayOfScope(r *http.Request) {

	vars := mux.Vars(r)
	mDayOfScope = vars["id"]

}
