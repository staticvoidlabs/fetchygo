package managers

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/staticvoidlabs/fetchy/models"
)

var mEpgRepository models.EpgRepository
var mRefreshSummary models.RefreshSummary
var mFilterLiveShows bool

func GetEpgData() {

	if mRefreshSummary.RefreshState == "Refreshing" {

		WriteLogMessage("Already refreshing -> skipping...", true)
		return
	}

	mRefreshSummary.RefreshState = "Refreshing"
	mRefreshSummary.CountChannels = 0
	mRefreshSummary.CountShowItems = 0

	mEpgRepository.EpgChannelRepos = nil
	mEpgRepository.RefreshAt = "cleared"
	mEpgRepository.Shows = 0

	WriteLogMessage("Refreshing EPG repositories...", false)

	tmpChannels := strings.Split(mCurrentConfig.Channels, ",")

	// Iterate ChannelIDs
	for _, channel := range tmpChannels {

		//mChannelURL := "https://www.hoerzu.de/text/tv-programm/sender.php?newday=0&tvchannelid=65&timeday=ganztags"

		var tmpEpgChannelRepo models.EpgChannelRepo
		tmpChannelInfo := strings.Split(channel, "_")
		tmpEpgChannelRepo.ChannelName = tmpChannelInfo[1]

		tmpURL := mCurrentConfig.BaseUrl + "?newday=" + mDayOfScope + "&tvchannelid=" + tmpChannelInfo[0] + "&timeday=" + mCurrentConfig.DayTime

		client := &http.Client{
			//CheckRedirect: redirectPolicyFunc,
		}
		req, err := http.NewRequest("GET", tmpURL, nil)
		req.Header.Add("Accept", `text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
		req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
		res, err := client.Do(req)
		//fmt.Println(res, err)

		if err != nil {
			fmt.Println(err)
		}
		if res.StatusCode != 200 {
			fmt.Println("Failed to get EPG data: " + res.Status)
		}
		defer res.Body.Close()

		// Load the HTML document
		doc, err := goquery.NewDocumentFromReader(res.Body)
		if err != nil {
			fmt.Println(err)
		}

		tmpCounter := 0
		var tmpShowIDs []string

		// Find the review items
		doc.Find("a").Each(func(i int, s *goquery.Selection) {

			// Get 'Live' flag
			tmpAttribute, err2 := s.Attr("name")

			if err2 {
				fmt.Println(err2)
			}

			if tmpAttribute != "" && tmpAttribute != "auswahlmenu" && tmpAttribute != "Zur Navigation" {
				tmpShowIDs = append(tmpShowIDs, strings.TrimSpace(tmpAttribute))
			}

			// Get show info
			tmpString := strings.TrimSpace(s.Text())

			if tmpString != "" && tmpString != "Zum Auswahlmenü" && tmpString != "Zur Navigation" {

				if mCurrentConfig.DebugLevel == 1 {
					fmt.Println(tmpCounter)
					fmt.Println("Current item: " + tmpShowIDs[tmpCounter] + " > " + s.Text())
				}

				var tmpEpgShowItem models.EpgShowItem

				tmpShowItem := strings.Split(strings.TrimSpace(s.Text()), " , ")

				tmpEpgShowItem.StartTime = tmpShowItem[0]
				tmpEpgShowItem.Title = tmpShowItem[1]
				tmpEpgShowItem.Genre = tmpShowItem[2]
				tmpEpgShowItem.Description = ""
				tmpEpgShowItem.Scoring = CalculateShowScoring(tmpEpgShowItem.Title)
				tmpEpgShowItem.Live = GetLiveAttributeByShowId(tmpShowIDs[tmpCounter], tmpEpgShowItem.Title)

				tmpEpgChannelRepo.EpgShowItems = append(tmpEpgChannelRepo.EpgShowItems, tmpEpgShowItem)

				if mCurrentConfig.DebugLevel == 1 {
					fmt.Println(tmpCounter)
					fmt.Println("Current item: " + tmpShowIDs[tmpCounter] + " > " + s.Text())
				}

				tmpCounter++
			}

		})

		mEpgRepository.EpgChannelRepos = append(mEpgRepository.EpgChannelRepos, tmpEpgChannelRepo)

		mRefreshSummary.CountShowItems += GetNumberOfShowItemsByChannel(tmpEpgChannelRepo)
	}

	mRefreshSummary.CountChannels = len(mEpgRepository.EpgChannelRepos)
	mRefreshSummary.TimeStamp = time.Now()
	mRefreshSummary.RefreshState = "Refreshed"

	WriteLogMessage("Refreshing EPG repositories done.", true)
}

func CalculateShowScoring(title string) int {

	tmpScoring := 0

	//tmpWhitelist := mFilter

	if mFilter != "" {

		tmpFilterElements := strings.Split(mFilter, ";")

		if tmpFilterElements != nil && len(tmpFilterElements) > 0 {

			for _, element := range tmpFilterElements {

				if strings.Contains(title, element) {
					tmpScoring = tmpScoring + 10
				}
			}

		}

	} else {
		tmpScoring = 000
	}

	return tmpScoring
}

func ChannelHasRelevantShowItems(channelRepo models.EpgChannelRepo) bool {

	tmphasRelevantShowItems := false

	if mFilter == "" {

		tmphasRelevantShowItems = true

	} else {

		for _, showItem := range channelRepo.EpgShowItems {

			if showItem.Scoring > 0 {
				tmphasRelevantShowItems = true
			}
		}

	}

	return tmphasRelevantShowItems
}

func GetNumberOfShowItemsByChannel(channelRepo models.EpgChannelRepo) int {

	tmpAmountOfRelevantShowItems := 0

	for _, showItem := range channelRepo.EpgShowItems {

		if showItem.Scoring >= mCurrentConfig.MinScoring {
			tmpAmountOfRelevantShowItems++
		}
	}

	return tmpAmountOfRelevantShowItems
}

func checkShowRelevance(showTitle string) bool {

	tmpIsRelevant := false

	if mFilter != "" {

		tmpFilterElements := strings.Split(mCurrentConfig.WhitelistTitleElements, ";")

		if tmpFilterElements != nil && len(tmpFilterElements) > 0 {

			for _, element := range tmpFilterElements {

				if strings.Contains(showTitle, element) {
					tmpIsRelevant = true
				}
			}
		}

	}

	return tmpIsRelevant
}

func GetLiveAttributeByShowId(showId string, showTitle string) bool {

	tmpIsLive := false
	tmpIsRelevantShow := checkShowRelevance(showTitle)

	if mFilterLiveShows && tmpIsRelevantShow {

		tmpURL := mCurrentConfig.BaseUrlDetails + "?broadcast_id=" + showId

		client := &http.Client{
			//CheckRedirect: redirectPolicyFunc,
		}
		req, err := http.NewRequest("GET", tmpURL, nil)
		req.Header.Add("Accept", `text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8`)
		req.Header.Add("User-Agent", `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11`)
		res, err := client.Do(req)

		if err != nil {
			fmt.Println(err)
		}
		if res.StatusCode != 200 {
			fmt.Println("Failed to get EPG details data: " + res.Status)

			return tmpIsLive
		}
		defer res.Body.Close()

		// Load the HTML document
		doc, err := goquery.NewDocumentFromReader(res.Body)
		if err != nil {
			fmt.Println(err)
		}

		doc.Find("BODY").Each(func(i int, s *goquery.Selection) {

			tmpString := strings.TrimSpace(s.Text())

			tmpIsLive1 := strings.Contains(tmpString, "Live")
			tmpIsLive2 := strings.Contains(tmpString, "LIVE")
			tmpIsLive3 := strings.Contains(tmpString, "live")

			if tmpIsLive1 || tmpIsLive2 || tmpIsLive3 {
				tmpIsLive = true
			}

		})

		if mCurrentConfig.DebugLevel == 1 {
			fmt.Println("Live flag for show '"+showTitle+"' > "+"%t", tmpIsLive)
		}

	}

	return tmpIsLive

}
