package managers

import (
	"fmt"
	"time"
)

var mLogMessages []string

func getTimeStamp() string {
	return " > " + time.Now().Format("2006/01/02 15:04:05") + "   "
}

// InitLogging resets/initializes the log stack.
func InitLogging() {
	mLogMessages = nil
}

// WriteLogMessage writes a message to the given output locations.
func WriteLogMessage(message string, nextLineEmpty bool) {

	// Append message to message stack.
	mLogMessages = append(mLogMessages, getTimeStamp()+message)
	fmt.Println(message)

	// Add empty line if needed.
	if nextLineEmpty {
		mLogMessages = append(mLogMessages, "")
		fmt.Println("")
	}

}

// GetCurrentLog returns the current log entries.
func GetCurrentLog() []string {

	return mLogMessages
}
