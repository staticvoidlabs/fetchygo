package models

import "time"

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version                 string `json:"version"`
	WebAPIPort              int    `json:"webAPIPort"`
	DebugLevel              int    `json:"debugLevel"`
	RefreshInterval         int32  `json:"refreshInterval"`
	ShowSummaryAfterRefresh bool   `json:"showSummaryAfterRefresh"`
	BaseUrl                 string `json:"baseUrl"`
	BaseUrlDetails          string `json:"baseUrlDetails"`
	Channels                string `json:"channels"`
	DayTime                 string `json:"dayTime"`
	WhitelistTitleElements  string `json:"whitelistTitleElements"`
	MinScoring              int    `json:"minScoring"`
}

type RefreshSummary struct {
	RefreshState   string
	TimeStamp      time.Time
	CountChannels  int
	CountShowItems int
	FilterSet      string
}

type EpgRepository struct {
	EpgChannelRepos0 []EpgChannelRepo
	EpgChannelRepos1 []EpgChannelRepo
	EpgChannelRepos2 []EpgChannelRepo
	CountShowItems   int
	RefreshAt        string
}

type EpgChannelRepo struct {
	ChannelName  string
	EpgShowItems []EpgShowItem
}

type EpgShowItem struct {
	Title       string
	Description string
	StartTime   string
	Genre       string
	Scoring     int
	Live        bool
}
