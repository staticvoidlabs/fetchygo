module gitlab.com/staticvoidlabs/fetchy

go 1.20

require github.com/PuerkitoBio/goquery v1.8.1

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/gorilla/mux v1.8.0
	golang.org/x/net v0.7.0 // indirect
	gorm.io/gorm v1.25.2
)
