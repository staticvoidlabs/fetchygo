package main

import (
	"gitlab.com/staticvoidlabs/fetchy/managers"
)

func main() {

	managers.ProcessConfigFile()
	managers.InitAPIService()

	/*
		for {

			managers.ProcessConfigFile()
			managers.GetEpgData()

			tmpRefrehInterval := managers.GetRefrehInterval()
			time.Sleep(time.Duration(rand.Int31n(tmpRefrehInterval)) * time.Second)

		}
	*/

}
