#!/bin/bash

echo "fetchy.service: ## Starting ##" | systemd-cat -p info

cd /home/alex/bin/fetchy
./fetchy

echo "fetchy.service: Shutting down." | systemd-cat -p info
